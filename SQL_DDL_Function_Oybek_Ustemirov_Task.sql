-- Task 1 

CREATE VIEW sales_revenue_by_category_qtr AS
select category.name as film_category, sum(p.amount) as sales_revenue from category
       left join film_category fc on category.category_id = fc.category_id
       left join film f on f.film_id = fc.film_id
       left join inventory i on f.film_id = i.film_id
       left join rental r on i.inventory_id = r.inventory_id
       left join payment p on r.rental_id = p.rental_id
where p.payment_date BETWEEN (timestamp '2017-03-01' - INTERVAL '3 month')::date AND now()::date
    group by category.category_id;

--for checking
select * from sales_revenue_by_category_qtr 





-- Task 2 

CREATE OR REPLACE FUNCTION get_sales_revenue_by_category_qtr(current_quarter integer)
RETURNS TABLE (
    film_category text,
    sales_revenue numeric
) AS
$$
BEGIN
RETURN QUERY
SELECT
    category.name AS film_category,
    COALESCE(sum(p.amount), 0) AS sales_revenue
FROM
    category
        LEFT JOIN film_category fc ON category.category_id = fc.category_id
        LEFT JOIN film f ON f.film_id = fc.film_id
        LEFT JOIN inventory i ON f.film_id = i.film_id
        LEFT JOIN rental r ON i.inventory_id = r.inventory_id
        LEFT JOIN payment p ON r.rental_id = p.rental_id
WHERE
    p.payment_date BETWEEN (timestamp '2017-03-01' - INTERVAL '3 months')::date AND now()::date
        AND EXTRACT(quarter FROM p.payment_date) = current_quarter
GROUP BY
    category.category_id, category.name;
END;
$$
LANGUAGE plpgsql;


--for checking
select * from get_sales_revenue_by_category_qtr(1)



-- Task 3 

CREATE OR REPLACE PROCEDURE new_movie(p_movie_title VARCHAR)
LANGUAGE plpgsql
AS $$
DECLARE
    v_language_id INT;
    v_film_id INT;
BEGIN
    -- Check if the language exists
    SELECT language_id INTO v_language_id
    FROM language
    WHERE name = 'Klingon';

    IF v_language_id IS NULL THEN
        RAISE EXCEPTION 'Language "Klingon" does not exist in the language table.';
    END IF;

    -- Generate a new unique film ID
    SELECT COALESCE(MAX(film_id), 0) + 1 INTO v_film_id
    FROM film;

    -- Insert the new movie into the film table
    INSERT INTO film (film_id, title, rental_rate, rental_duration, replacement_cost, release_year, language_id)
    VALUES (v_film_id, p_movie_title, 4.99, 3, 19.99, EXTRACT(YEAR FROM CURRENT_DATE), v_language_id);

    COMMIT;
END;
$$;

--for checking
CALL new_movie('KING III');

